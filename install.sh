#!/bin/bash
echo " +-------------------------------------------------------+"$'\n' \
  $'|   NetHunter bootanimation installer 1.0 by yesimxev   |'$'\n' \
  $'^=======================================================^'$'\n'
echo "Mounting system.."
mount -o rw,remount /system || exit_on_error "Failed to mount system! If your device is system_as_root, please run mount -o rw,remount / in AndroidSU terminal! Exiting.."
echo "Copying files .."
cp output/bootanimation.zip /system/media/
echo "Setting permissions.."$'\n'
chmod o+r /system/media/bootanimation.zip
echo "Bootanimation is installed! Please restart your device to check it out"
